function [f_ros,jac_frose]=F_Rose(x)
n=14;
iter=1;
f_ros=[];
jac_frose=[];

while(iter<n)
    f_ros=[f_ros,10*(x(iter+1)-x(iter)^2),1-x(iter)];
    i=2;
    gradf1=[];
    gradf2=[];
    while(i<iter)
        gradf1=[gradf1,0,0];
        gradf2=[gradf2,0,0];
        i=i+2;
    end
    gradf1=[gradf1,-20*x(1)];
    gradf1=[gradf1,10];
    gradf2=[gradf2,-1,0];
    while(i<n)
        gradf1=[gradf1,0,0];
        gradf2=[gradf2,0,0];
        i=i+2;
    end
    jac_frose=[jac_frose,gradf1',gradf2'];
    iter=iter+2;
end
f_ros=f_ros';
jac_frose=jac_frose';
    
    