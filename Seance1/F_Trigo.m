function [f_trigo,jac_ftrigo]=F_Trigo(x)
n=14;
iter=1;
f_trigo=[];
jac_ftrigo=[];

while(iter<n+1)
    j=1;
    s=0;
    while(j<n)
        s=s+cos(x(j))+iter*(1-cos(x(iter)))-sin(x(iter));
        j=j+1;
    end
    f_trigo=[f_trigo,n-s];
    gradf=[];
    i=1;
    while(i<n+1)
        if(i==iter)
            gradf=[gradf,sin(x(iter))-n*(iter*sin(x(iter))-cos(x(iter)))];
        else
            gradf=[gradf,sin(x(i))];
        end
        i=i+1;
    end
    jac_ftrigo=[jac_ftrigo,gradf'];
    iter=iter+1;
end
f_trigo=f_trigo';
jac_ftrigo=jac_ftrigo';
    