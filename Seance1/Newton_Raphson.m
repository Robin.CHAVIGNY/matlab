function [x,f,Flag,iter]=Newton_Raphson(F,x0,eps)
Flag=0; %tout c'est bien passée
iter=0;
x=x0;
[f,jac_f]=feval(F,x);
w=-inv(jac_f)*f;
err=norm(w)/norm(x);

while (iter <200 && err>eps)
    [f,jac_f]=feval(F,x);
    w=-inv(jac_f)*f;
    x=x+w;
    iter=iter+1;
    err=norm(w)/norm(x);
end

