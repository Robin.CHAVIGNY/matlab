function d=matd(x,y)
%
% Matrice de distance d'un ensemble de points 
%
%
n=length(x);

% replication de x et y en n colonnes 
xx=x(:,ones(1,n));
yy=y(:,ones(1,n));

% calcul des distances
d=sqrt((xx-xx').^2+(yy-yy').^2);