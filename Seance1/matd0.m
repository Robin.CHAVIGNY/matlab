function d=matd0(x,y)
n=length(x);
d=zeros(n);

%boucle de calcul des distances
for i=1:n
    for j=1:n
        d(i,j)=sqrt((x(i)-x(j))^2+(y(i)-y(j))^2);
    end
end
