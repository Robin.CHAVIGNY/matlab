function [x,fx,flag,iter]=newtonraphson(fun,x0,tol,itmax,kprint)
%% NEWTONRAPHSON: résolution d'un système non linéaire
%
% Synopsis:
%      [x,fx,flag,iter]=newtonraphson(fun,x0,tol,itmax,kprint)
%      [x,fx,flag,iter]=newtonraphson(fun,x0,tol,itmax)
%      [x,fx,flag,iter]=newtonraphson(fun,x0,tol)
%      [x,fx,flag,iter]=newtonraphson(fun,x0)
%
% Inputs:
%       fun : variable fonction du système, (variable fonction, @fct)
%             [fx,Jf]=fct(x)
%       x0  : valeur initiale de x (vecteur)
%       tol : tolérance des calculs (double)
%             arrêt des itérations si ||f(x)||<tol
%             valeur par défaut 10^-6
%     itmax : nombre d'itérations max (entier)
%             valur par défaut 100
%    kprint : indicateur d'impression (entier)
%             0 - pas d'impression, valeur par défaut
%             1 - impression des itérations 0 et finale
%             2 - impression de |f(x)| et de l'erreur relative sur x à
%                 toutes les iterations
%
% Outputs:
%      x   : solution du problème (vecteur)
%      fx  : valeur de f à la solution (vecteur)
%     flag : indicateur de fin de calcul (entier)
%            0 - tout s'est bien passé (arrêt normal, |f(x)|<tol)
%            1 - erreur relative sur x
%            2 - le nombre max d'itérations atteint
%            3 - la matrice jacobienne est devenue singulière
%     iter : nombre d'itérations (entier)
%-------------------------------------------------------------------------

if (nargin == 2)
    tol=19^-6; itmax=100; kprint=0;
elseif (nargin == 3)
    itmax=100; kprint=0;
elseif (nargin == 4)
     kprint=0;
end

x=x0;

[fx,~]=feval(fun,x);

flag=0;
iter = 0;
err = 1;

if (kprint > 0)
    fprintf("iter=%d  |f(x)|=%15.8e \n",iter,norm(fx))
end

while (iter < itmax & err > tol)
    iter = iter+1;
    
    [fx,Jf]=feval(fun,x);
    
    % Factorisation de Jf
    [L,U]=lu(Jf);
    
    % Test de singularité sur Jf
    d=prod(diag(U));
    if (abs(d) < 10^-14)
        flag = 3;
        break
    end
    
    % Mise à jour x
    w=-U\(L\fx);
    x=x+w;
    
    % Test d'arrêt
    err=norm(w)/norm(x);
    
    if (kprint > 1)
       fprintf("iter=%d  |f(x)|=%15.8e e-relative=%15.8e \n",iter,...
           norm(fx),err)  
    end
end

if (kprint > 0)
    fprintf("iter=%d  |f(x)|=%15.8e e-relative=%15.8e \n",iter,...
           norm(fx),err)  
end

if (flag == 2)
    return
end

if (iter == itmax)
    flag=2;
else
    if (err < tol & norm(fx) > tol)
        flag = 1;
    end
end