function []=mandel(omega,nbpoint,seuil,itmax)
%% Mandelbrot : Création de fractales
% Inputs:
%   omega:   matrie 2x2 avec: première ligne borne du segment pour les x
%                             deuxième ligne borne du segment pour les y
%   nbpoint: vecteur ligne contenent les taille de tabulation
%            respectivement pour x et y
%   seuil:   valeur pour laquelle la suite (un) est considérer divergente
%   itmax:   valeur maximal du nombre d'itération faite pour le calcul de
%            (un)
%--------------------------------------------------------------------------


if (nargin == 0)
    omega=[-2 2 ;-2 2];nbpoint=[1/100 1/100];itmax=30;seuil=2;
end

x=omega(1,1):nbpoint(1):omega(1,2);
y=omega(2,1):nbpoint(2):omega(2,2);
x=x';
y=y';

A=zeros((omega(1,2)-omega(1,1))*(1/(nbpoint(2)))+1,(omega(2,2)-omega(2,1))*(1/(nbpoint(1)))+1);

l=0;

while (l<(omega(1,2)-omega(1,1))*(1/(nbpoint(2)))+1)
    l=l+1;
    j=0;
    while (j<(omega(2,2)-omega(2,1))*(1/(nbpoint(2)))+1)
        j=j+1;
        u=0;
        n=0;
        z=x(l)+i*y(j);
        while (n<itmax)
            u=u^2 +z;
            if (abs(u)>seuil)
                A(l,j)=n;
                break;
            end
            n=n+1;
            if (n==itmax)
                A(l,j)=itmax;
            end
        end
    end
end

imagesc(x,y,A)
colormap jet