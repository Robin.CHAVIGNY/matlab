function [Cell]=ward(X)
%% WARD: classification ascendante hiérarchique 
%
% Inputs:
%       X        : matrice contenant n individus (les colonnes) avec
%                  leurs p variables (les lignes)
% Outputs:
%       Cell     : cellule de matrice des groupes d'individus proches à
%                  chaque itérations de boucles
%
%--------------------------------------------------------------------

% Initialisation
[~,n]=size(X);
clusters=[1:n;zeros(n-1,n)];
Cell={};

% Etape 1: Calcul de la première matrice des distances
s = sum(X.^2);
e=ones(n,1);
s=s';
D=s*e'+e*s'-2*X'*X;

% Etape 2: Début de l'algorithme   
k=1;
while(k<n)
    % Trouver les deux clusters les plus proches
    m=[inf,0,0];
    i=1;
    while(i<=n)
        j=i+1;
        while(j<=n)
            if(m(1)>D(i,j) && D(i,j)>0)
                m=[D(i,j),i,j];
            end
            j=j+1;
        end
        i=i+1;
    end
    
    
    % Fusion des clusters
    j=1;
    while(j<=n)
        if(clusters(j,m(2))==0)
            J=j;
            break;
        else
            j=j+1;
        end
    end
    U=n-J+1;
    
    % Mis à jour des clusters 
    clusters(J:n,m(2)) = clusters(1:U,m(3));
    clusters(:,m(3)) = zeros(1, n);
    
    % Implémentation de l'état des clusters à l'etape k
    Cell={Cell{:} clusters};
    
    %Lance-Williams
    l=1;
    while(l<=n)
        j=1;
        while(j<=n)
            if(clusters(j,l)==0)
                nl=j;
                break;
            else
                j=j+1;
            end
        end
        j=1;
        while(j<=n)
            if(clusters(j,m(2))==0)
                nij=j;
                break;
            else
                j=j+1;
            end
        end
        ni=J;
        nj=nij-ni;
        
        D(l,m(2))=((ni+nl)/(ni+nj+nl))*D(m(2),l)+((nj+nl)/(ni+nj+nl))*D(m(3),l)-(nl/(ni+nj+nl))*D(m(2),m(3));
        D(m(2),l)=D(l,m(2));
        
        l=l+1;
    end
    
    % Mis à jour matrice des distances
    D(m(3),:)=zeros(1,n);
    D(:,m(3))=zeros(n,1);
    
    k=k+1;
end
